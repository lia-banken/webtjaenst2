package com.example.webservice2;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
public class Group {
    private String id;
    private String groupName;
    private List<String> members;
/*dgthethdfvsdfvsdfvrthr*/
    @JsonCreator
    public Group(@JsonProperty("id") String id,
                 @JsonProperty("groupName") String groupName,
                 @JsonProperty("members") List<String> members) {
        this.id = id;
        this.groupName = groupName;
        this.members = members;
    }

    public void deleteMember(String id) {
        members.removeIf(member -> member.equals(id));
    }
}
