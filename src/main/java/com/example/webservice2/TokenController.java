package com.example.webservice2;


import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/token")
@AllArgsConstructor
@CrossOrigin
public class TokenController {

    TokenService tokenService;

    @PostMapping("/create-Token")
    public Token createToken(){
        return tokenService.createToken();
    }

    @GetMapping("/getAllToken")
    public List<Token> getAllToken(){
        return tokenService.getAllTokens().collect(Collectors.toList());
    }
}
